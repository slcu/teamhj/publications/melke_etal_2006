## Files

The <tt>ensemble.data</tt> file holds the 32 parameter sets analysed in the paper. 
Each row holds one set of parameters p_0..., and the parameter sets are divided 
into the two groups of solutions analysed.<br>

The <tt>tgfb.model</tt> file is a model description that can be used as input to 
the [organism](https://gitlab.com/slcu/teamhj/organism) software for simulating the model.

## Contact

henrik.jonsson@slcu.cam.ac.uk
